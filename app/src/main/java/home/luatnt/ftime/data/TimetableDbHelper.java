package home.luatnt.ftime.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TimetableDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ftime.db";
    private static final int DATABASE_VERSION = 1;

    public TimetableDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_TIMETABLE_TABLE = "CREATE TABLE" + TimetableContact.TimetableEntry.TABLE_NAME + " ("
                + TimetableContact.TimetableEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TimetableContact.TimetableEntry.KEY_TITLE + " TEXT, "
                + TimetableContact.TimetableEntry.KEY_DATE + " TEXT, "
                + TimetableContact.TimetableEntry.KEY_TIME + " TEXT, "
                + TimetableContact.TimetableEntry.KEY_REPEAT + " TEXT, "
                + TimetableContact.TimetableEntry.KEY_REPEAT_NO + " TEXT, "
                + TimetableContact.TimetableEntry.KEY_REPEAT_TYPE + " TEXT, "
                + TimetableContact.TimetableEntry.KEY_ACTIVE + " TEXT, " + " );";

        sqLiteDatabase.execSQL(SQL_CREATE_TIMETABLE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
